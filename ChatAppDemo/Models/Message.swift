//
//  Message.swift
//  ChatAppDemo
//
//  Created by Ahmed Adel on 5/10/22.
//

import Foundation

struct Message:Identifiable,Codable{
    var id:String
    var text:String
    var recieved:Bool
    var timestamp: Date
}
