//
//  MessageBubble.swift
//  ChatAppDemo
//
//  Created by Ahmed Adel on 5/10/22.
//

import SwiftUI

struct MessageBubble: View {
    var message:Message
    @State private var showTime = false
    
    var body: some View {
        VStack(alignment: message.recieved ? .leading : .trailing){
            HStack{
                Text(message.text)
                    .padding()
                    .background(message.recieved ? Color("Gray") : Color("Peach"))
                    .cornerRadius(50)
            }
            .frame(maxWidth:300 , alignment: message.recieved ? .leading : .trailing)
            .onTapGesture {
                showTime.toggle()
            }
            
            if showTime{
                Text("\(message.timestamp.formatted(.dateTime.hour().minute()))")
                    .font(.caption2)
                    .foregroundColor(.gray)
                    .padding(message.recieved ? .leading : .trailing,25)
            }
        }
        .frame(maxWidth:.infinity , alignment: message.recieved ? .leading : .trailing)
        .padding(message.recieved ? .leading : .trailing)
        .padding(.horizontal, 10)
    }
}

struct MessageBubble_Previews: PreviewProvider {
    static var previews: some View {
        MessageBubble(message:Message(id: "123", text: "Hi,My name is Ahmed Adel", recieved: false, timestamp: Date.now))
    }
}
