//
//  ChatAppDemoApp.swift
//  ChatAppDemo
//
//  Created by Ahmed Adel on 5/10/22.
//

import SwiftUI
import Firebase

@main
struct ChatAppDemoApp: App {
    
    init(){
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
